﻿using UnityEngine;

public class SheepShopGUI : MonoBehaviour {

    public GameObject blockDestoyerScript;
    public GameObject submarineGUIScript;

    private GameObject ShopGui;

    void Start () {
        ShopGui = GameObject.FindGameObjectWithTag("ShopGUI");
    }
	
	void Update () {
        var submarineHui = submarineGUIScript.GetComponent<SubmarineGUI>();
        if ((int)Vector3.Distance(submarineHui.transform.position, submarineHui.limit.position) == 0)
        {
            ShopGui.SetActive(true);
        }
        else
        {
            ShopGui.SetActive(false);
        }
	}
}
