﻿using UnityEngine;

public class GUIControl : MonoBehaviour {

    public void moveUp()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineMovement>().moveUp();
    }

    public void moveDown()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineMovement>().moveDown();
    }

    public void moveRight()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineMovement>().moveRight();
    }

    public void moveLeft()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineMovement>().moveLeft();
    }
}
