﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimpleMapGenerator : MonoBehaviour
{

	public int x;
	public int y;
	public List<Transform> prefab;

	private readonly int firstLayer = 10;
	private readonly int secondLayer = 60;

	private readonly ArrayList distribution1 = new ArrayList();
	private readonly ArrayList distribution2 = new ArrayList();
	private readonly ArrayList distribution3 = new ArrayList();

	void Start ()
	{
		distribution1.Add (70);
		distribution1.Add (25);
		distribution1.Add (5);
		distribution2.Add (10);
		distribution2.Add (60);
		distribution2.Add (30);
		distribution3.Add (5);
		distribution3.Add (35);
		distribution3.Add (60);

		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				Instantiate (CreateObjectByRandom (j), new Vector2 (transform.position.x + i * 10.01f, transform.position.y - j * 10.01f), Quaternion.identity);
			}
		}
	}

	void Update ()
	{

	}

	private Transform CreateObjectByRandom (int deep)
	{
		int[] level = GetDeepLevel (deep);
		return getObject (level);
	}

	private Transform getObject (int[] level)
	{
		int _value = (int)Random.Range (0, 100) + 1;
		if (_value <= level [0]) {
			return prefab [0];
		} else if (_value > level [0] && _value <= (level [0] + level [1])) {
			return prefab [1];
		} else if (_value > (level [0] + level [1])) {
			int reosurceIndex = (int)Random.Range (2, 5);
			return prefab [reosurceIndex];
		}
		throw new UnityException ("Incorrect calculation");
	}

	private int[] GetDeepLevel (int currentDeep)
	{
		int level = currentDeep * 100 / y;
		if (level <= firstLayer) {
			return (int[])distribution1.ToArray(typeof(int)) as int[];
		} else if (level > firstLayer && level <= (secondLayer + firstLayer)) {
			return (int[])distribution2.ToArray(typeof(int)) as int[];
		} else if (level > (secondLayer + firstLayer)) {
			return (int[])distribution3.ToArray(typeof(int)) as int[];
		}
		throw new UnityException ("Incorrect calculation");
	}
}
