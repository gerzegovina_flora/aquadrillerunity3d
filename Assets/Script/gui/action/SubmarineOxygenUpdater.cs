﻿using System;
using UnityEngine;

public class SubmarineOxygenUpdater : MonoBehaviour
{
    private SubmarineMovement submarineMovement;

    void Start()
    {
        submarineMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineMovement>();
    }

    public void SetMinimalContainer()
    {
        SubmarineGUI submarineGui = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>();
        if (Int32.Parse(submarineGui.Money) >= 10)
        {
            Oxygen oxygen = new Oxygen(500, "Default");
            submarineMovement.Oxygen = oxygen;
        }
    }

    public void SetMediumContainer()
    {
        SubmarineGUI submarineGui = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>();
        if (Int32.Parse(submarineGui.Money) >= 10)
        {
            Oxygen oxygen = new Oxygen(1000, "Default");
            submarineMovement.Oxygen = oxygen;
        }
    }

    public void SetMaximumContainer()
    {
        SubmarineGUI submarineGui = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>();
        if (Int32.Parse(submarineGui.Money) >= 10)
        {
            Oxygen oxygen = new Oxygen(2500, "Default");
            submarineMovement.Oxygen = oxygen;
        }
    }
}
