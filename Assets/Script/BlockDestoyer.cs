﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class BlockDestoyer : BlockData {

    public List<RawMaterial> rawMaterials = new List<RawMaterial>();

    private SubmarineGUI submarineGui;

	void Start () {
		submarineGui = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>();
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (RAW_MATERIALS.ContainsKey(coll.gameObject.tag))
        {
            if (Int32.Parse(submarineGui.Container) <= Int32.Parse(submarineGui.MaxContainer))
            {
                RawMaterial rawMaterial = RAW_MATERIALS[coll.gameObject.tag];
                rawMaterials.Add(rawMaterial);
                submarineGui.Container = rawMaterials.Count.ToString();
                UpdateDrillerTemperature(rawMaterial);
            }
            GameObject.Destroy(coll.gameObject);
        }
        else
        {
            GameObject.Destroy(coll.gameObject);
        }
    }

    private void UpdateDrillerTemperature(RawMaterial rawMaterial)
    {
        double temperature = Double.Parse(submarineGui.drillerTemperature.GetComponent<Text>().text);
        submarineGui.drillerTemperature.GetComponent<Text>().text = (temperature + rawMaterial.Resistance).ToString();
    }
}
