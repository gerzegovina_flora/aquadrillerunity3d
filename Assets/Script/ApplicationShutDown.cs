﻿using UnityEngine;

public class ApplicationShutDown : MonoBehaviour
{

    private SubmarineMovement submarineMovement;

    void Start()
    {
        submarineMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineMovement>();
    }

    void OnApplicationQuit()
    {
        Engine engine = submarineMovement.Engine;
        Drill drill = submarineMovement.Drill;
        Oxygen oxygen = submarineMovement.Oxygen;
        Container container = submarineMovement.Container;

        GameContextLoader.SaveAll(engine, drill, oxygen, container);
    }

}
