﻿using UnityEngine;
using System.Collections.Generic;

public class GUIShop : MonoBehaviour {

    private GameObject drillerList;
    private GameObject engineList;
	private GameObject containerList;

    void Start () {
        drillerList = GameObject.FindGameObjectWithTag("DrillerUpdatesList");
        engineList = GameObject.FindGameObjectWithTag("EngineUpdatesList");
		containerList = GameObject.FindGameObjectWithTag("ContainerUpdatesList");
    }
	
	void Update () {
	
	}

    public void activeDrillerMenu()
    {
        diactivateAllMenu();
        drillerList.SetActive(true);
    }

    public void activeEngineMenu()
    {
        diactivateAllMenu();
        engineList.SetActive(true);
    }

	public void activeContainerMenu()
	{
		diactivateAllMenu();
		containerList.SetActive(true);
	}

	public void Repair(){

	}

	public void SellAllItems(){
        SubmarineGUI submarineGui = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>();
        int money = int.Parse(submarineGui.Money);
		
        List<BlockDestoyer.RawMaterial> items = GameObject.FindGameObjectWithTag ("Container").GetComponent<BlockDestoyer> ().rawMaterials;
		foreach(BlockDestoyer.RawMaterial item in items){
			money += item.Cost;
		}
        items.Clear();

		submarineGui.Money = money.ToString();
        submarineGui.Container = "0";
	}

    private void diactivateAllMenu()
    {
        GameObject go = GameObject.FindGameObjectWithTag("DrillerUpdatesList");
        if (go != null)
        {
            go.SetActive(false);
        }
        go = GameObject.FindGameObjectWithTag("EngineUpdatesList");
        if (go != null)
        {
            go.SetActive(false);
        }
		go = GameObject.FindGameObjectWithTag("ContainerUpdatesList");
		if (go != null)
		{
			go.SetActive(false);
		}
    }
}
