﻿using UnityEngine;
using System;

public class SubmarineEngineUpdater : MonoBehaviour {

    private SubmarineMovement submarine;

    void Start () {
        submarine = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineMovement>();
    }

    public void SetDieselEngine()
    {
        SubmarineGUI submarineGui = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>();
        if (Int32.Parse(submarineGui.Money) >= 10)
        {
            Engine engine = new Engine(1.5f, "Diesel");
            submarine.Engine = engine;
            submarineGui.Money = (Int32.Parse(submarineGui.Money) - 200).ToString();
        }
    }

    public void SetGasEngine()
    {
        SubmarineGUI submarineGui = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>();
        if (Int32.Parse(submarineGui.Money) >= 10)
        {
            Engine engine = new Engine(3.5f, "Gas");
            submarine.Engine = engine;
            submarineGui.Money = (Int32.Parse(submarineGui.Money) - 200).ToString();
        }
    }

    public void SetAtomicEngine()
    {
		SubmarineGUI submarineGui = GameObject.FindGameObjectWithTag ("Player").GetComponent<SubmarineGUI> ();
		if(Int32.Parse(submarineGui.Money) >= 10){
        	Engine engine = new Engine(12.5f, "Atomic");
        	submarine.Engine = engine;
		}
    }

}
