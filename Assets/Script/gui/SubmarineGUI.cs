﻿using UnityEngine;
using UnityEngine.UI;

public class SubmarineGUI : MonoBehaviour {

    public Transform limit;

	public GameObject oxygen;
	public GameObject drillerTemperature;
	public GameObject container;
	public GameObject maxOxygen;
	public GameObject maxDrillerTemperature;
	public GameObject maxContainer;
	public GameObject money;

    private Text moneyText;
	private Text oxygenText;
	private Text drillerTemperatureText;
	private Text containerText;
	private Text maxOxygenText;
	private Text maxDrillerTemperatureText;
	private Text maxContainerText;

    void Start () {
		moneyText = money.GetComponent<Text> ();
		oxygenText = oxygen.GetComponent<Text> ();
		drillerTemperatureText = drillerTemperature.GetComponent<Text> ();
		containerText = container.GetComponent<Text> ();
		maxOxygenText = maxOxygen.GetComponent<Text> ();
		maxDrillerTemperatureText = maxDrillerTemperature.GetComponent<Text> ();
		maxContainerText = maxContainer.GetComponent<Text> ();

		moneyText.text = "0";
		oxygenText.text = "0";
		drillerTemperatureText.text = "0";
		containerText.text = "0";

		maxOxygenText.text = gameObject.GetComponent<SubmarineMovement>().Oxygen.MaxOxygen.ToString();
		maxDrillerTemperatureText.text = gameObject.GetComponent<SubmarineMovement>().Drill.MaxTemperature.ToString();
		maxContainerText.text = gameObject.GetComponent<SubmarineMovement>().Container.MaxItems.ToString();
	}
	
	void Update () {
        CalculateDistance();

    }

    private void CalculateDistance()
    {
        Deep = limit.position.y - transform.position.y;
    }

    public float Deep { get; set; }

    public string Money
	{
		get
		{
			return moneyText.text;
		}
		set
		{
			moneyText.text = value;
		}
	}

	public string Oxygen
	{
		get
		{
			return oxygenText.text;
		}
		set
		{
			oxygenText.text = value;
		}
	}

	public string MaxOxygen
	{
		get
		{
			return maxOxygenText.text;
		}
		set
		{
			maxOxygenText.text = value;
		}
	}

	public string Temperature
	{
		get
		{
			return drillerTemperatureText.text;
		}
		set
		{
			drillerTemperatureText.text = value;
		}
	}

	public string MaxTemperature
	{
		get
		{
			return maxDrillerTemperatureText.text;
		}
		set
		{
			maxDrillerTemperatureText.text = value;
		}
	}

	public string Container
	{
		get
		{
			return containerText.text;
		}
		set
		{
			containerText.text = value;
		}
	}

	public string MaxContainer
	{
		get
		{
			return maxContainerText.text;
		}
		set
		{
			maxContainerText.text = value;
		}
	}

}
