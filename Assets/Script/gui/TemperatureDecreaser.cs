﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TemperatureDecreaser : MonoBehaviour
{

    void Update()
    {
        double temperature = Double.Parse(GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>()
            .drillerTemperature.GetComponent<Text>().text);

        if (temperature > 0)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>()
                .drillerTemperature.GetComponent<Text>().text = (temperature - Time.deltaTime).ToString();
        }
    }
}
