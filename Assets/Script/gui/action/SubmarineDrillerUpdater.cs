﻿using UnityEngine;
using System.Collections;
using System;

public class SubmarineDrillerUpdater : MonoBehaviour {

	private SubmarineMovement submarineMovement;

	void Start () {
        submarineMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineMovement>();
	}
	
	public void SetThunderDrill()
	{
		SubmarineGUI submarineGui = GameObject.FindGameObjectWithTag ("Player").GetComponent<SubmarineGUI> ();
		if(Int32.Parse(submarineGui.Money) >= 10){
			Drill drill = new Drill(1, "Thunder", 8);
            submarineMovement.Drill = drill;
			Debug.Log ("Drill bought");
		}
	}

	public void SetAtomicDrill()
	{
        SubmarineGUI submarineGui = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>();
        if (Int32.Parse(submarineGui.Money) >= 10)
        {
            Drill drill = new Drill(2, "Atomic", 8);
            submarineMovement.Drill = drill;
            Debug.Log("Drill bought");
        }
    }

	public void SetUltraDrill()
	{
        SubmarineGUI submarineGui = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>();
        if (Int32.Parse(submarineGui.Money) >= 10)
        {
            Drill drill = new Drill(4, "Ultra", 8);
            submarineMovement.Drill = drill;
            Debug.Log("Drill bought");
        }
    }
}
