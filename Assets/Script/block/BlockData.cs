﻿using System.Collections.Generic;
using UnityEngine;

public class BlockData: MonoBehaviour
{

    public static Dictionary<string, RawMaterial> RAW_MATERIALS = new Dictionary<string, RawMaterial>() {
        {"Gold", new RawMaterial("Gold", 200, 5, 10) },
        {"Silver", new RawMaterial("Silver", 200, 6, 10) },
        {"Platium", new RawMaterial("Platium", 200, 6, 10) },
        {"Coal", new RawMaterial("Coal", 200, 5, 10) },
        {"Uran", new RawMaterial("Uran", 200, 10, 10) },
        {"Iron", new RawMaterial("Iron", 200, 6, 10)}
    };

    public class RawMaterial
    {
        public RawMaterial(string name, int cost, int hardness, int resistance)
        {
            this.Cost = cost;
            this.Name = name;
            this.Hardness = hardness;
            this.Resistance = resistance;
        }

        public string Name { get; set; }

        public int Cost { get; set; }

        public int Hardness { get; set; }

        public int Resistance { get; set; }

    }
}
