﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class OxygenDecreaser : MonoBehaviour
{

    private SubmarineGUI submarineGUI;

    private void Start()
    {
        submarineGUI = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>();
    }

    void Update()
    {
        double oxygen = Double.Parse(submarineGUI.oxygen.GetComponent<Text>().text);
        if (oxygen > 0 && submarineGUI.Deep > 0)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineGUI>()
                .oxygen.GetComponent<Text>().text = (oxygen - Time.deltaTime).ToString();
        }
    }
}
