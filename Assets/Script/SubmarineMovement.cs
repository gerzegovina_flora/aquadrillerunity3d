﻿using UnityEngine;

public class SubmarineMovement : BlockData
{

    public Transform rightTarget;
    public Transform leftTarget;
    public Transform upTarget;
    public Transform downTarget;

    private bool isMove;
    private Transform oldRightPosition;
    private Transform oldLeftPosition;
    private Transform oldUpPosition;
    private Transform oldDownPosition;

    private bool _moveUp;
    private bool _moveDown;
    private bool _moveRight;
    private bool _moveLeft;

    private float tempSpeed;

    void Start ()
    {
        Engine = GameContextLoader.LoadEngine();
        Drill = GameContextLoader.LoadDrill();
        Oxygen = GameContextLoader.LoadOxygen();
        Container = GameContextLoader.LoadContainer();
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (RAW_MATERIALS.ContainsKey(coll.gameObject.tag))
        {
            float speedDecreasor = RAW_MATERIALS[coll.gameObject.tag].Hardness;
            tempSpeed = Engine.Speed;
            Engine.Speed = (Engine.Speed + Drill.Capacity) - speedDecreasor;
        }
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        if (RAW_MATERIALS.ContainsKey(coll.gameObject.tag))
        {
            Engine.Speed = tempSpeed;
        }
    }

    void Update()
    {
        if (!isMove) {
            if (_moveLeft)
            {
                oldLeftPosition = Instantiate(leftTarget, new Vector2(leftTarget.position.x, leftTarget.position.y), Quaternion.identity) as Transform;
                isMove = true;
            }
            if (_moveUp)
            {
                oldUpPosition = Instantiate(upTarget, new Vector2(upTarget.position.x, upTarget.position.y), Quaternion.identity) as Transform;
                isMove = true;
            }
            if (_moveRight)
            {
                oldRightPosition = Instantiate(rightTarget, new Vector2(rightTarget.position.x, rightTarget.position.y), Quaternion.identity) as Transform;
                isMove = true;
            }
            if (_moveDown)
            {
                oldDownPosition = Instantiate(downTarget, new Vector2(downTarget.position.x, downTarget.position.y), Quaternion.identity) as Transform;
                isMove = true;
            }
        }
        if (isMove) {
            float step = _getSpeed();
            if (oldRightPosition != null && transform.position != oldRightPosition.position)
            {
                transform.position = Vector2.MoveTowards(transform.position, oldRightPosition.position, step);
            }
            else
            if (oldLeftPosition != null && transform.position != oldLeftPosition.position)
            {
                transform.position = Vector2.MoveTowards(transform.position, oldLeftPosition.position, step);
            }
            else
            if (oldUpPosition != null && transform.position != oldUpPosition.position)
            {
                transform.position = Vector2.MoveTowards(transform.position, oldUpPosition.position, step);
            }
            else
            if (oldDownPosition != null && transform.position != oldDownPosition.position)
            {
                transform.position = Vector2.MoveTowards(transform.position, oldDownPosition.position, step);
            }
            else
            {
                _moveUp = _moveDown = _moveRight = _moveLeft = isMove = false;
            }
        }
        else
        {
            if (oldRightPosition != null) {
                GameObject.Destroy(oldRightPosition.gameObject);
                oldRightPosition = null;
            }
            if (oldLeftPosition != null)
            {
                GameObject.Destroy(oldLeftPosition.gameObject);
                oldLeftPosition = null;
            }
            if (oldDownPosition != null)
            {
                GameObject.Destroy(oldDownPosition.gameObject);
                oldDownPosition = null;
            }
            if (oldUpPosition != null)
            {
                GameObject.Destroy(oldUpPosition.gameObject);
                oldUpPosition = null;
            }
            isMove = false;
        }
    }

    private float _getSpeed()
    {
        return Engine.Speed * Time.deltaTime;
    }

    public void moveUp()
    {
        _moveUp = true;
    }

    public void moveDown()
    {
        _moveDown = true;
    }

    public void moveRight()
    {
        _moveRight = true;
    }

    public void moveLeft()
    {
        _moveLeft = true;
    }

    public Engine Engine { get; set; }

    public Drill Drill { get; set; }

    public Oxygen Oxygen { get; set; }

    public Container Container { get; set; }
}

public class Engine
{
    public Engine(float speed, string name)
    {
        this.Speed = speed;
        this.Name = name;
    }

    public float Speed { get; set; }

    public string Name { get; set; }
}

public class Drill
{
    public Drill(int capacity, string name, int maxTemperature)
    {
        this.Capacity = capacity;
        this.Name = name;
		this.MaxTemperature = maxTemperature;
    }

    public int MaxTemperature { get; set; }

    public int Capacity { get; set; }

    public string Name { get; set; }
}

public class Oxygen
{
    public Oxygen(int maxOxygen, string name)
	{
		this.MaxOxygen = maxOxygen;
        this.Name = name;
    }

    public int MaxOxygen { get; set; }

    public string Name { get; set; }
}

public class Container
{
    public Container(int maxItems, string name)
	{
		this.MaxItems = maxItems;
        this.Name = name;
    }

    public int MaxItems { get; set; }

    public string Name { get; set; }
}