﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    private GameObject menuMap;
    private GameObject mainMenu;

    public void Start()
    {
        menuMap = GameObject.FindGameObjectWithTag("MenuMap");
        mainMenu = GameObject.FindGameObjectWithTag("MainMenu");
        menuMap.SetActive(false);
    }

    public void NewGame()
    {
        menuMap.SetActive(true);
		gameObject.SetActive (false);
    }

    public void Couintinue()
    {

    }

}
