﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameContextLoader : MonoBehaviour
{

    private const string ENGINE_SPEED = "engine.speed";
    private const string ENGINE_NAME = "engine.name";

    private const string DRILL_CAPACITY = "drill.capacity";
    private const string DRILL_NAME = "drill.name";
    private const string DRILL_MAX_TEMPERATURE = "drill.max.temperature";

    private const string OXYGEN_CAPACITY = "oxygen.capacity";
    private const string OXYGEN_NAME = "oxygen.name";

    private const string CONTAINER_CAPACITY = "container.size";
    private const string CONTAINER_NAME = "container.name";

    public static Engine LoadEngine()
    {
        if (PlayerPrefs.HasKey(ENGINE_NAME))
        {
            float speed = PlayerPrefs.GetFloat(ENGINE_SPEED);
            string name = PlayerPrefs.GetString(ENGINE_NAME);
            return new Engine(speed, name);
        }
        return new Engine(10f, "Default");
    }

    internal static Container LoadContainer()
    {
        if (PlayerPrefs.HasKey(DRILL_NAME))
        {
            int capacity = PlayerPrefs.GetInt(CONTAINER_CAPACITY);
            string name = PlayerPrefs.GetString(CONTAINER_NAME);
            return new Container(capacity, name);
        }
        return new Container(1000, "Default");
    }

    public static void SaveEngine(Engine engine)
    {
        PlayerPrefs.SetFloat(ENGINE_SPEED, engine.Speed);
        PlayerPrefs.SetString(ENGINE_NAME, engine.Name);
    }

    public static Drill LoadDrill()
    {
        if (PlayerPrefs.HasKey(DRILL_NAME)) 
        { 
            int capacity = PlayerPrefs.GetInt(DRILL_CAPACITY);
            int maxTemperature = PlayerPrefs.GetInt(DRILL_MAX_TEMPERATURE);
            string name = PlayerPrefs.GetString(DRILL_NAME);
            return new Drill(capacity, name, maxTemperature);
        }
        return new Drill(5, "Default", 15);
    }

    public static void SaveDrill(Drill drill)
    {
        PlayerPrefs.SetInt(DRILL_CAPACITY, drill.Capacity);
        PlayerPrefs.GetInt(DRILL_MAX_TEMPERATURE, drill.MaxTemperature);
        PlayerPrefs.GetString(DRILL_NAME, drill.Name);
    }

    public static void SaveAll(Engine engine, Drill drill, Oxygen oxygen, Container container)
    {
        PlayerPrefs.SetFloat(ENGINE_SPEED, engine.Speed);
        PlayerPrefs.SetString(ENGINE_NAME, engine.Name);

        PlayerPrefs.SetInt(DRILL_CAPACITY, drill.Capacity);
        PlayerPrefs.GetInt(DRILL_MAX_TEMPERATURE, drill.MaxTemperature);
        PlayerPrefs.GetString(DRILL_NAME, drill.Name);

        PlayerPrefs.SetInt(OXYGEN_CAPACITY, oxygen.MaxOxygen);
        PlayerPrefs.SetString(OXYGEN_NAME, oxygen.Name);

        PlayerPrefs.SetInt(CONTAINER_CAPACITY, container.MaxItems);
        PlayerPrefs.SetString(CONTAINER_NAME, container.Name);
    }

    public static Oxygen LoadOxygen()
    {
        if (PlayerPrefs.HasKey(DRILL_NAME))
        {
            int capacity = PlayerPrefs.GetInt(OXYGEN_CAPACITY);
            string name = PlayerPrefs.GetString(DRILL_NAME);
            return new Oxygen(capacity, name);
        }
        return new Oxygen(1000, "Default");
    }
}
