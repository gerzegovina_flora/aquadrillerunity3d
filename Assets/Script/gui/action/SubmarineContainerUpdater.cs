﻿using UnityEngine;
using System.Collections;
using System;

public class SubmarineContainerUpdater : MonoBehaviour {

	private SubmarineMovement submarine;

	void Start () {
		submarine = GameObject.FindGameObjectWithTag("Player").GetComponent<SubmarineMovement>();
	}

	public void SetMiddleContainer()
	{
		SubmarineGUI submarineGui = GameObject.FindGameObjectWithTag ("Player").GetComponent<SubmarineGUI> ();
		if(Int32.Parse(submarineGui.Money) >= 200){
			Container container = new Container(10, "Default");
			submarine.Container = container;
			Debug.Log ("Container bought");
		}
	}

	public void SetBigContainer()
	{

	}

	public void SetHugeContainer()
	{

	}
}
